" Anne's VIM configuration file

"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=~/.local/share/dein/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('~/.local/share/dein')
  call dein#begin('~/.local/share/dein')

  " Let dein manage dein
  " Required:
  call dein#add('~/.local/share/dein/repos/github.com/Shougo/dein.vim')

  " Tree explorer
  call dein#add('scrooloose/nerdtree')
  call dein#add('Xuyuanp/nerdtree-git-plugin')

  " Collection of language packs
  call dein#add('sheerun/vim-polyglot')

  " Color theme
  call dein#add('nanotech/jellybeans.vim')

  " Automatic alignment (e.g align '=')
  call dein#add('junegunn/vim-easy-align')

  " Status bar
  call dein#add('vim-airline/vim-airline')
  call dein#add('vim-airline/vim-airline-themes')

  " Completion
  call dein#add('Shougo/deoplete.nvim')
  if !has('nvim')
    call dein#add('roxma/nvim-yarp')
    call dein#add('roxma/vim-hug-neovim-rpc')
  endif
  call dein#add('Shougo/neosnippet.vim')
  call dein#add('Shougo/neosnippet-snippets')

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif

"End dein Scripts-------------------------

" Enable deoplete
let g:deoplete#enable_at_startup = 1

" Display and format line numbers.
set number
set numberwidth=4

" Enable UTF-8 (I wanna see Umlauts!).
set encoding=utf8

" Display tabs and trailing whitespaces.
set list
set listchars=tab:→\ ,eol:\ ,trail:·

" Without any syntax highlighting, programming is a pain:
syntax on

" Keybindings -----------------------------------------------------------------

" Leader key
let mapleader="\<Space>"

" Disable Arrow keys in Escape mode
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" Move between splits
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Balance splits
nnoremap  <silent><leader>w <c-w>=

" Toggle colorcolumn
function! g:ToggleColorColumn()
  if &colorcolumn != 0
    setlocal colorcolumn=0
  else
    setlocal colorcolumn=80
  endif
endfunction
nnoremap <silent> <leader>h :call g:ToggleColorColumn()<CR>

" Toggle NerdTree
map <C-n> :NERDTreeToggle<CR>

" Neosnippet superTab like snippets behavior.
imap <expr><TAB>
 \ pumvisible() ? "\<C-n>" :
 \ neosnippet#expandable_or_jumpable() ?
 \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" Misc ------------------------------------------------------------------------

filetype plugin indent on

" Mouse support ("all")
set mouse=a

" Show command and mode.
set showcmd

" Auto indentation.
set autoindent
set copyindent

" Use spaces instead of tabs.
set expandtab
set tabstop=2
set shiftwidth=2

" More natural splits
set splitbelow
set splitright

" Colorscheme
colorscheme jellybeans

" Vim-airline: display status bar
set laststatus=2
" Vim-airline: automatically displays all buffers when there's only one tab
" open.
" let g:airline#extensions#tabline#enabled = 1
